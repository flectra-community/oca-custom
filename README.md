# Flectra Community / oca-custom

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[project_members](project_members/) | 1.0.1.0.0| add members to projects
[oca_custom](oca_custom/) | 1.0.1.1.0| Custom Settings for OCA Instance
[website_oca_psc_team](website_oca_psc_team/) | 1.0.1.0.0| Displays PSC Teams in website.
[oca_psc_team](oca_psc_team/) | 1.0.1.0.0| This module allows you to store PSC details in project.These details will be displayed in PSC Teams in website.
[oca_event_badge](oca_event_badge/) | 1.0.1.0.0|         Creates Custom Event Badges based on the Partner Tags
[website_oca_integrator](website_oca_integrator/) | 1.0.1.0.1| Displays Integrators in website.


